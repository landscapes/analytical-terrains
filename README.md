# Physically-based analytical erosion for fast terrain generation

### [Project Page](https://www-sop.inria.fr/reves/Basilic/2024/TGSC24/) | [Paper](https://www-sop.inria.fr/reves/Basilic/2024/TGSC24/Analytical_Terrains_EG.pdf)

This repository contains the source code for the paper "Physically-based analytical erosion for fast terrain generation"

## Dependencies

The code requires ```numpy``` and ```numba```

Even though the code can be used as is, [Houdini](https://www.sidefx.com/products/houdini/) is used for visualization and interactivity. Houdini's python installation should already have ```numpy```. ```numba``` (or any other package) can be installed by opening a shell from inside Houdini and using pip:

```
python -m pip
```

## Examples

The Houdini file contains example terrains. For running ```standalone_example.py``` data are available [here](https://www-sop.inria.fr/reves/Basilic/2024/TGSC24/analytical_terrains_examples.zip).

## Citation

```bibtex
@article{TGSC24,
    author = {Tzathas, Petros and Gailleton, Boris and Steer, Philippe and Cordonnier, Guillaume},
    title = {Physically-based analytical erosion for fast terrain generation},
    journal = {Computer Graphics Forum},
    volume = {43},
    number = {2},
    pages = {e15033},
    doi = {https://doi.org/10.1111/cgf.15033},
    url = {https://onlinelibrary.wiley.com/doi/abs/10.1111/cgf.15033},
    eprint = {https://onlinelibrary.wiley.com/doi/pdf/10.1111/cgf.15033},
    year = {2024}
}
```

## Acknowledgements and Funding

This project was sponsored by the Agence Nationale de la Recherche project Invterra ANR-22-CE33-0012-01, by the H2020 European Research Council (grant agreement no. 803721), and research and software donations from Adobe Inc. 
