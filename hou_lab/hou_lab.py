#
# Copyright (C) 2024, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
#
# This software is free for non-commercial, research and evaluation use
# under the terms of the LICENSE.md file.
#
# For inquiries contact  petros.tzathas@inria.fr
#

import sys

experiment_package = 'analytical'
experiment_package_path = f'../{experiment_package}'
sys.path.append(experiment_package_path)

import importlib

import hou_lab.import_tracker as import_tracker


import_tracker.start_tracking(experiment_package)


def reload():
    import_tracker.reload()


def update(exp_name, data):
    data = importlib.import_module(experiment_package + '.' + exp_name).run(data)

    return data
