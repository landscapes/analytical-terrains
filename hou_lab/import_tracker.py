#
# Copyright (C) 2024, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
#
# This software is free for non-commercial, research and evaluation use
# under the terms of the LICENSE.md file.
#
# For inquiries contact  petros.tzathas@inria.fr
#

import importlib
from importlib.abc import MetaPathFinder, Loader
from importlib.util import spec_from_loader
import sys

import os
import inspect


class HLabReloader(Loader):

    def __init__(self, reload_dict, module_name):
        self.reload_dict = reload_dict
        self.module_name = module_name

    def create_module(self, spec):
        module = self.reload_dict[self.module_name]
        sys.modules[spec.name] = module
        module =  importlib.reload(module)

        # only remove module from reload dict if it successfully imported
        del self.reload_dict[self.module_name]

        return module

    def exec_module(self, module):
        pass


class HLabTrackerPathFinder(MetaPathFinder):

    def __init__(self, module_name = None):
        super().__init__()

        #backup sys meta_path 
        self.meta_path = [ t for t in sys.meta_path]

        # module name to track
        self.module_name = module_name

        #set of local packages
        self.local_names = set()
        self.local_packages = set()

        # modules waiting for reload
        self.modules_waiting_reload = {}

    def find_spec(self, fullname, path, target=None):

        # do nothing on reload
        if target is not None:
            return None

        # check if the module is waiting for reload
        if fullname in self.modules_waiting_reload:
            loader = HLabReloader(self.modules_waiting_reload, fullname)
            return spec_from_loader(fullname, loader)

        else:

            for mp in self.meta_path:
                if hasattr(mp, 'find_spec'):
                    spec = mp.find_spec(fullname, path, target)
                    if spec is not None:

                        module_is_local = False

                        if self.module_name is None:
                            module_is_local = spec.origin.startswith(os.path.abspath(os.curdir) + os.sep)
                        else:
                            module_is_local = spec.name == self.module_name or spec.name.startswith(self.module_name+'.')
                        
                        if module_is_local:
                            self.local_names.add(spec.name)
                            if '.' in spec.name:
                                self.local_packages.add('.'.join(spec.name.split('.')[:-1]))

                        return spec

        return None

    def trigger_reload(self):

        # remove local imports from sys, stagging them for reload
        del_from_local = []
        for m in self.local_names:
            if m in sys.modules:
                self.modules_waiting_reload[m] = sys.modules[m]
                del sys.modules[m]
            # self.modules_waiting_reload can still hold m if a parent module failed to load
            # to check: is it possible that m disapeared from both module and sys.modules? seems unlikely
            elif m not in self.modules_waiting_reload: 
                del_from_local.append(m)
        
        if len(del_from_local) > 0:
            self.local_names = set([m for m in self.local_names if m not in del_from_local])
                
        # if modules are left in a package dict, they are not reloaded after "from pkg import module".
        # to prevent that, we remove all modules from  the dict of local packages
        for m in [ self.modules_waiting_reload[n] for n in self.local_packages if n in self.modules_waiting_reload]:
            for k, v in list(m.__dict__.items()):
                if inspect.ismodule(v):
                    del m.__dict__[k]
            



def start_tracking(module_name = None):
    
    if start_tracking._meta_path_finder is None:
        start_tracking._meta_path_finder = HLabTrackerPathFinder(module_name)

    sys.meta_path.insert(0, start_tracking._meta_path_finder)

start_tracking._meta_path_finder = None



def reload():
    if start_tracking._meta_path_finder is not None:
        start_tracking._meta_path_finder.trigger_reload()
    