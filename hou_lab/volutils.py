#
# Copyright (C) 2024, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
#
# This software is free for non-commercial, research and evaluation use
# under the terms of the LICENSE.md file.
#
# For inquiries contact  petros.tzathas@inria.fr
#

"""
Some utilities for handling volumes in houdini
this script is made to be called directly from houdini with python2
"""

import numpy as np
import hou


def _get_prim(geo=None, field=None, prim=None):
    """
    find volume primitive. see h_to_np for more detail
    """

    if prim is not None and field is not None:
        raise ValueError("Please give only one of 'prim' or 'field' arguments")
    if prim is not None and geo is not None:
        raise ValueError("Please give only one of 'prim' or 'geo' arguments")

    # find primitive
    if prim is None:
        if geo is None:
            geo = hou.pwd().geometry()

        if field is not None and geo.findPrimAttrib('name') is None:
            raise ValueError("The geometry does not have a 'name' attribute")

        for p in geo.prims():
            if isinstance(p, hou.Volume):
                if field is None or p.attribValue('name') == field:
                    prim = p
                    break

        if prim is None:
            raise ValueError("No primitive found")

    return prim


def _resolution_to_shape(r):
    shape = r

    if shape[-1] == 1:
        if shape[-2] == 1:
            shape = shape[0]
        else:
            shape = (shape[1], shape[0])
    else:
        shape = (shape[2], shape[1], shape[0])

    return shape


def vol_shape(geo=None, field=None, prim=None):
    prim = _get_prim(geo, field, prim)

    return _resolution_to_shape(prim.resolution())


def h_to_np(geo=None, field=None, prim=None):
    """
    Create a np array from a houdini volume
    The primitive can be directly given (with the prim argument) or given as the cobination of a geometry (geo) and an attribute name (field)
    If the geometry is not given, it will be guessed from the current node. If only the geometry is given, the first volume will be chosen
    """

    prim = _get_prim(geo, field, prim)

    shape = _resolution_to_shape(prim.resolution())

    return np.frombuffer(prim.allVoxelsAsString(), dtype=np.float32).reshape(shape)


def np_to_new_h(data, geo=None, field=None, bbox=None):
    """
    Create a new houdini volume from numpy 'data'
    The volume will be created as part of the 'geo' geometry (from the current node if geo is not specified)
    If 'field' is given, this will be the value of the 'name' attirbute of the primitive
    """

    if geo is None:
        geo = hou.pwd().geometry()

    if len(data.shape) == 1:
        data = data.reshape(1, data.shape[0])

    if len(data.shape) == 2:
        data = data.reshape(1, data.shape[0], data.shape[1])

    # create new volume
    if bbox is None:
        bbox = hou.BoundingBox(-.5, -.5, -.5, .5, .5, .5)

    volume = geo.createVolume(data.shape[2], data.shape[1], data.shape[0], bbox)
    volume.setAllVoxelsFromString(data.astype(np.float32))

    if field is not None:
        name_attrib = geo.findPrimAttrib('name')
        if name_attrib is None:
            name_attrib = geo.addAttrib(hou.attribType.Prim, "name", "") 
        volume.setAttribValue(name_attrib, field)

    return volume


def update_volume(data, geo=None, field=None, prim=None):

    """
    update an existing houdini volume with np data
    see h_to_np to see how the primitive is chosen
    """

    prim = _get_prim(geo, field, prim)

    # check resolution
    shape = prim.resolution()

    if len(data.shape) == 1:
        data = data.reshape(1, data.shape[0])

    if len(data.shape) == 2:
        data = data.reshape(1, data.shape[0], data.shape[1])

    if shape[0] != data.shape[2] or shape[1] != data.shape[1] or shape[2] != data.shape[0]:
        raise ValueError('Resolution mismatch')

    prim.setAllVoxelsFromString(data.astype(np.float32))
