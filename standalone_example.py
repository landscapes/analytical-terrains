import os
import glob
import json

import numpy as np

from analytical import analytical_multigrid


input_folder = os.path.join("data", "eg_fast_forward")
output_folder = input_folder

param_file = os.path.join(input_folder, "params.txt")
with open(param_file) as f:
    params = json.load(f)

npy_files = glob.glob("*.npy", root_dir=input_folder)
maps = {}
for npy_file in npy_files:
    map_name = os.path.splitext(npy_file)[0]
    maps[map_name] = np.load(os.path.join(input_folder, npy_file))

# Combine params and maps in single dictionary
# Order is important as boundary map overwrites boundary type in params
data = params | maps

# # Set different time
# data["t"] = 1e6

analytical_multigrid.run(data)

for map_name in ["height", "drain", "receivers"]:
    np.save(
        os.path.join(output_folder, f"{map_name}.npy"),
        data[map_name],
    )
