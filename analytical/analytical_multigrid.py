#
# Copyright (C) 2024, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
#
# This software is free for non-commercial, research and evaluation use
# under the terms of the LICENSE.md file.
#
# For inquiries contact  petros.tzathas@inria.fr
#

import numpy as np
import numba 

import analytical.drain as drain
import analytical.depressions as depressions
from analytical.order import OrderedTopo
from analytical.analytical_solution import solve_analytically
from analytical.cliff_optimization import optimize_cliffs


def run(data):
    np.random.seed(0)

    # Model parameters
    # fmt: off
    initial_elevation = data["initial_height"]
    dx                = data["dx"]
    t                 = data["t"]
    uplift            = data["uplift"]
    k_spl             = data["k_spl"]
    k_hill            = data["k_hillslope"]
    k_thermal         = data["k_thermal"]
    m_spl             = data["m_spl"]
    m_hill            = data["m_hillslope"]
    bounds            = data["boundary"]
    out_slope         = data["out_slope"]
    # fmt: on

    bounds = data["boundary"]
    if isinstance(bounds, np.ndarray):
        bounds = bounds > 0.9
        if not np.any(bounds):
            bounds = "all_sides"

    rcv_mode = "single_rnd_fixed"

    advection_mode = data["advection_mode"]
    if advection_mode == "spl":
        advection_speed_fn = spl_advection_speed
    elif advection_mode == "hydrology":
        advection_speed_fn = hydrology_advection_speed
    else:
        raise ValueError(f"Invalid advection speed mode {advection_mode}")

    initial_elevation = depressions.fill_holes(OrderedTopo(initial_elevation))

    multigrid_levels = data["multigrid_levels"]
    multigrid_levels = max(multigrid_levels, 1)
    multigrid_levels = min(multigrid_levels, initial_elevation.shape[0].bit_length() - 1)

    check_resolution_n_levels_consistency(initial_elevation.shape[0], multigrid_levels)
    check_resolution_n_levels_consistency(initial_elevation.shape[1], multigrid_levels)

    solution_iters, solution_level, smoothing_iters = (
        data["solution_iters"],
        data["solution_level"],
        data["smoothing_iters"],
    )

    solution_iters = max(solution_iters, 1)

    solution_level = max(solution_level, 0)
    solution_level = min(solution_level, multigrid_levels - 1)

    smoothing_iters = max(smoothing_iters, 0)

    print(f'Number of levels: {multigrid_levels}')
    print(f'Solution level: {solution_level}')
    print(f'Solution iterations: {solution_iters}')
    print(f'Smoothing iterations: {smoothing_iters}')
    print(f'Time: {t}')

    # Downsample parameter maps
    params = [initial_elevation, uplift, k_spl, k_hill, k_thermal, m_spl, m_hill]

    multi_params = len(params) * [None]
    for i, param in enumerate(params):
        multi_params[i] = multigrid_levels * [None]
        multi_param = multi_params[i]

        multi_param[0] = param
        for level in range(1, multigrid_levels):
            multi_param[level] = downsample(multi_param[level - 1])

    (
        multi_initial_elevation,
        multi_uplift,
        multi_k_spl,
        multi_k_hill,
        multi_k_thermal,
        multi_m_spl,
        multi_m_hill,
    ) = multi_params

    # Handle downsampling bounds separately
    multi_bounds = multigrid_levels * [bounds]
    if isinstance(bounds, np.ndarray):
        for level in range(1, multigrid_levels):
            multi_bounds[level] = multi_bounds[level - 1][::2, ::2]

    nlevels = multigrid_levels - solution_level

    elevation, drainage_area, topo = v_cycle(
        nlevels,
        multi_initial_elevation[solution_level:],
        multi_uplift[solution_level:],
        multi_k_spl[solution_level:],
        multi_k_hill[solution_level:],
        multi_k_thermal[solution_level:],
        multi_m_spl[solution_level:],
        multi_m_hill[solution_level:],
        dx * 2**solution_level, 
        t,
        rcv_mode,
        multi_bounds[solution_level:],
        advection_speed_fn,
        out_slope,
        solution_iters,
    )

    for level in reversed(range(solution_level)):
        elevation = upsample(elevation)

    if solution_level != 0 or rcv_mode == "multi":
        surface = depressions.fill_holes(OrderedTopo(elevation, bounds))
        topo = OrderedTopo(surface, bounds, rcv_mode="single_min")

    if solution_level != 0:
        precipitation = np.ones_like(elevation)
        drainage_area = drain.compute_drain(topo, dx, precipitation)

    # Smooth steep cliffs
    elevation = optimize_cliffs(elevation, topo, smoothing_iters, learning_rate=0.01)

    receivers = topo.rcv()

    lakes = np.zeros_like(elevation)

    data.update({
        'height': elevation,
        'lake': lakes,
        'drain': drainage_area,
        'receivers': receivers[:, 0]
    })

    return data


def spl_advection_speed(drainage_area, k_spl, k_hill, m_spl, m_hill):
    return k_spl * (drainage_area**m_spl) + k_hill * (drainage_area**m_hill)


def hydrology_advection_speed(drainage_area, k_spl, k_hill, m_spl, m_hill):
    high_speed = 0.1 / 6
    low_speed = 0.01 / 6
    drainage_threshold = np.quantile(drainage_area, 0.945) # 0.92

    # print(f'Drainage threshold: {drainage_threshold}')
    
    return high_speed * (drainage_area >= drainage_threshold) + low_speed * (drainage_area < drainage_threshold)


def v_cycle(
    nlevels,
    multi_initial_elevation,
    multi_uplift,
    multi_k_spl,
    multi_k_hill,
    multi_k_thermal,
    multi_m_spl,
    multi_m_hill,
    dx,
    t,
    rcv_mode,
    multi_bounds,
    advection_speed_fn,
    out_slope,
    iter_per_level,
):
    elevation = np.copy(multi_initial_elevation[nlevels-1])

    for level in reversed(range(nlevels)):
        level_dx = dx * 2**level
        precipitation = np.ones_like(elevation)

        # This serves to increase the drainage for a point
        # acting as the source of the river carving the canyon
        if (
            isinstance(multi_bounds[level], str)
            and multi_bounds[level] == "single_node"
        ):
            precipitation[-1, 2 * (precipitation.shape[1] // 3)] = 20e3

        for i in range(iter_per_level):
            elevation = depressions.fill_holes(
                OrderedTopo(elevation, multi_bounds[level])
            )
            topo = OrderedTopo(elevation, multi_bounds[level], rcv_mode=rcv_mode)
            drainage_area = drain.compute_drain(topo, level_dx, precipitation)

            slope_correction = topo.slope_correction()
            slope_correction += slope_correction == 0

            level_initial_elevation = depressions.fill_holes_from_topo(
                topo, multi_initial_elevation[level]
            )

            elevation_t, _response_time, _path_length = solve_analytically(
                level_initial_elevation,
                drainage_area,
                multi_uplift[level],
                multi_k_spl[level],
                multi_k_hill[level],
                multi_k_thermal[level],
                multi_m_spl[level],
                multi_m_hill[level],
                level_dx / slope_correction,
                t,
                topo,
                advection_speed_fn,
                out_slope,
            )
            elevation = 0.75 * elevation + 0.25 * elevation_t

        if level != 0:
            elevation = upsample(elevation)

    return elevation, drainage_area, topo


def check_resolution_n_levels_consistency(base_resolution, resolution_levels):
    scale = 1 << (resolution_levels - 1)

    if base_resolution % scale != 0:
        raise ValueError(
            f"The number of resolution levels ({resolution_levels}) specified cannot be used with the given resolution ({base_resolution})"
        )


@numba.njit(cache=True)
def downsample(z):
    res_x, res_y = z.shape
    downsampled_z = np.empty((res_x // 2, res_y // 2))

    for y in range(downsampled_z.shape[1]):
        for x in range(downsampled_z.shape[0]):
            downsampled_z[y, x] = 0.25 * (
                z[2 * y, 2 * x]
                + z[2 * y, 2 * x + 1]
                + z[2 * y + 1, 2 * x]
                + z[2 * y + 1, 2 * x + 1]
            )

    return downsampled_z


upsampling_jitter = {}


def upsample(z):
    upsampled_shape = tuple(2 * i for i in z.shape)
    if upsampled_shape not in upsampling_jitter:
        r = 0.25
        jitter = np.random.uniform(low=-r, high=r, size=(*upsampled_shape, 2))

        upsampling_jitter[upsampled_shape] = jitter

    return _numba_upsample(z, upsampling_jitter[upsampled_shape])


@numba.njit(cache=True)
def _numba_upsample(z, jitter):
    res_x, res_y = z.shape
    upsampled_z = np.zeros((res_x * 2, res_y * 2))

    def lerp(z0, z1, t):
        return z0 + (z1 - z0) * t

    def bilerp(z00, z01, z10, z11, tx, ty):
        return lerp(lerp(z00, z01, tx), lerp(z10, z11, tx), ty)

    for y in range(upsampled_z.shape[1]):
        for x in range(upsampled_z.shape[0]):
            dx, dy = jitter[y, x]

            xf = x + 0.5 + dx
            yf = y + 0.5 + dy

            x_d, x_m = divmod(xf - 1, 2)
            y_d, y_m = divmod(yf - 1, 2)
            x_d = int(x_d)
            y_d = int(y_d)

            z00 = z[y_d, x_d] if x_d >= 0 and y_d >= 0 else 0
            z01 = z[y_d, x_d + 1] if x_d < res_x - 1 and y_d >= 0 else 0
            z10 = z[y_d + 1, x_d] if x_d >= 0 and y_d < res_y - 1 else 0
            z11 = z[y_d + 1, x_d + 1] if x_d < res_x - 1 and y_d < res_y - 1 else 0

            upsampled_z[y, x] = bilerp(z00, z01, z10, z11, x_m / 2, y_m / 2)

            # Nearest neighbor
            # upsampled_z[y, x] = bilerp(z00, z01, z10, z11, int(x_m > 1), int(y_m > 1))

    return upsampled_z
