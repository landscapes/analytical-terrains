#
# Copyright (C) 2024, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
#
# This software is free for non-commercial, research and evaluation use
# under the terms of the LICENSE.md file.
#
# For inquiries contact  petros.tzathas@inria.fr
#

import numpy as np
import numba


class OrderedTopo:

    def __init__(self, z, bounds="all_sides", rcv_mode="multi"):
        """
        z : elevation
        bounds: either one of "all_sides", "single_sides", "single_node"
                or a boolean array of same shape as z
        rcv_mode: 'multi', 'single_min', 'single_rnd'

        """
        self.z = z
        self.shape = z.shape
        self.eps = 1e-7

        if isinstance(bounds, str):
            bounds = create_bounds_from_boundary_type(bounds, z.shape)
        self.bounds = bounds

        self.rcv_mode = rcv_mode
        if self.rcv_mode not in ('multi', 'single_min', 'single_rnd', 'single_rnd_fixed'):
            raise ValueError(f"rcv_mode {rcv_mode} is not valid. Should be 'multi', 'single_min' or 'single_rnd'")

    def rcv(self):
        if not hasattr(self, '_rcv'):
            self._compute_rcv()

        return self._rcv

    def don(self):
        if not hasattr(self, '_don'):
            self._compute_don()

        return self._don

    def weights(self):
        if not hasattr(self, '_weights'):
            self._compute_weights()

        return self._weights

    def stack(self):
        if not hasattr(self, '_stack'):
            self._compute_stack()

        return self._stack

    def _compute_rcv(self):
        # ordering (explicitely discard 0-weighted edges)
        x_incr = np.pad(self.z[:, 1:] > self.z[:, :-1], ((0, 0), (1,1)))
        x_decr = np.pad(self.z[:, 1:] < self.z[:, :-1], ((0, 0), (1,1)))

        y_incr = np.pad(self.z[1:, :] > self.z[:-1, :], ((1,1), (0, 0)))
        y_decr = np.pad(self.z[1:, :] < self.z[:-1, :], ((1,1), (0, 0)))

        # rcv
        is_rcv = np.stack((x_incr[:, :-1], x_decr[:, 1:], y_incr[:-1, :], y_decr[1:, :]), axis = -1).reshape(-1, 4)
        is_rcv[self.bounds.reshape(-1)] = False

        idt = np.arange(self.z.reshape(-1).shape[0])
        idt4 = idt[:, None].repeat(4, axis = 1)
        self._idt, self._idt4 = idt, idt4

        self._rcv = idt4 + is_rcv * np.array([-1, 1, -self.shape[1], self.shape[1]]).reshape(1, 4)

        # save the multi recevier connectivity
        self._rcv4 = self._rcv.copy()

        if self.rcv_mode == 'single_min':
            self._rcv_id = np.argmin(self.z.reshape(-1)[self._rcv], axis = -1)
            self._rcv = self._rcv[idt, self._rcv_id][:, None]

        elif self.rcv_mode == 'single_rnd' or self.rcv_mode == 'single_rnd_fixed':
            W = self.z.reshape(-1, 1) - self.z.reshape(-1)[self._rcv]
            proba = W.cumsum(axis = -1)

            sampling = proba - self._random(self._rcv.shape[0]) * proba[:, -1:]

            self._rcv_id = (sampling >0).argmax(axis = -1)
            self._rcv = self._rcv[idt, self._rcv_id][:, None]

    def _random(self, length):
        if self.rcv_mode == 'single_rnd':
            return np.random.random((length, 1))
        else: #self.rcv_mode == 'single_rnd_fixed':
            if not hasattr(OrderedTopo, "static_sampling"):
                OrderedTopo.static_sampling = {}
            if length not in OrderedTopo.static_sampling:
                OrderedTopo.static_sampling[length] = np.random.random((length, 1))
            return OrderedTopo.static_sampling[length]

    def _compute_weights(self):
        W = self.z.reshape(-1)[:, None] - self.z.reshape(-1)[self.rcv()]
        self._weights = W / W.sum(axis=-1, keepdims=True).clip(self.eps)

    def _compute_don(self):
        # make sure rcv are computed
        rcv = self.rcv()
        self._don = self._idt4.copy()

        if self.rcv_mode != 'multi':
            rcv = self._idt4.copy()
            rcv[self._idt[:, None], self._rcv_id[:, None]] = self.rcv()

        for (r, d) in ((0, 1), (1, 0), (2, 3), (3, 2)):
            rcv_pos = np.where(rcv[:, r] != self._idt)[0]
            if rcv_pos.shape[0]>0:
                self._don[rcv[rcv_pos, r], d] = rcv_pos

    def check_rcv(self):
        # check if rcv are all lower
        if not np.all((self.z.reshape(-1, 1) > self.z.reshape(-1)[self.rcv()]) | (self.rcv() == self._idt[:, None])):
            raise RuntimeError('Receivers are not stricly lower')

        # check if all not bound nodes have at least a receiver
        if not np.all((self.rcv() != self._idt[:, None]).any(axis = 1) | self.bounds.reshape(-1)):
            raise RuntimeError('A not bound node has no receiver')

        # check if all bound nodes have no receiver
        if np.any((self.rcv() != self._idt[:, None]).any(axis = 1) & self.bounds.reshape(-1)):
            raise RuntimeError('A bound node has a receiver')

    def check_don(self):
        # check if rcv are all lower
        if not np.all((self.z.reshape(-1, 1) < self.z.reshape(-1)[self.don()]) | (self.don() == self._idt[:, None])):
            raise RuntimeError('Donnors are not stricly above')

    def _compute_stack(self):
        graph_bound_id = np.where(self.bounds.reshape(-1))[0]
        self._stack = np.empty_like(self._idt)
        self._stack[:graph_bound_id.size] = graph_bound_id

        num_rcv = np.sum((self.rcv() != self._idt[:, None]).astype(np.intp), axis = -1)

        _numba_order_nodes(self._stack, graph_bound_id.size, self.don(), num_rcv, np.empty_like(self._idt))

    def grad(self, z = None):
        # compute the (unoriented) downtream gradient of the topography, in an array of size (*shape, 2)
        # always use the rcv of 'topo', but uses either z is niot None, or self.z
        z = self.z if z is None else z

        dz = z.reshape(-1)[:, None] - z.reshape(-1)[self._rcv4]
        is_rcv = self._idt4 != self._rcv4
        num_x = is_rcv[..., 0] + is_rcv[..., 1]
        num_y = is_rcv[..., 2] + is_rcv[..., 3]

        return np.stack(
            [
                (dz[..., 0] + dz[..., 1]) / (num_x + (num_x == 0)),
                (dz[..., 2] + dz[..., 3]) / (num_y + (num_y == 0)),
            ],
            axis=-1,
        ).reshape(*z.shape, 2)

    def grad_norm(self, z = None):
        dz = self.grad(z)
        return np.sqrt((dz*dz).sum(-1))

    def velocity_correction(self, z = None):
        z = self.z if z is None else z
        dz = z.reshape(-1)[:, None] - z.reshape(-1)[self._rcv4]

        sum_dz = dz.sum(-1).reshape(z.shape)

        return self.grad_norm(z) / (sum_dz + (sum_dz == 0))

    def slope_correction(self, z = None):
        z = self.z if z is None else z
        dz = (z.reshape(-1)[:, None] - z.reshape(-1)[self._rcv]).sum(-1).reshape(z.shape)
        num_rcv = (self._idt[:, None] != self._rcv).sum(-1).reshape(z.shape)
        dz = dz / (num_rcv + (num_rcv==0))
        return self.grad_norm(z) / (dz + (dz == 0))

    def drain_correction(self, Q):
        Q[1:-1, 1:-1] = np.maximum(
            np.maximum(Q[:-2, 1:-1], Q[2:, 1:-1]),
            np.maximum(Q[1:-1, :-2], Q[1:-1, 2:])
        )
        return Q

    def enforce_receivers(self, river_mask, enforced_receivers):
        np.copyto(self.rcv(), enforced_receivers.reshape(-1, 1), where=river_mask.reshape(-1, 1))

        # Need to fix _rcv_id because it is used in _compute_don
        rcv_diff = self.rcv()[:, 0] - self._idt
        self._rcv_id = (rcv_diff > 0) + 2 * (rcv_diff % self.shape[1] == 0)

        # Recompute donnors and stack
        self._compute_don()
        self._compute_stack()


@numba.njit(cache = True)
def _numba_order_nodes(parse, stack_size, don, num_rcv, visibility):
    visibility[:] = 0
    parse_ptr = parse.size-1

    while stack_size:

        stack_size -= 1
        node = parse[stack_size]
        assert stack_size <= parse_ptr, "stack_size <= parse_ptr before adding in parse"
        parse[parse_ptr] = node
        parse_ptr-=1

        for nb in don[node]:
            if nb != node:
                visibility[nb] += 1
                if visibility[nb] == num_rcv[nb]:
                    assert stack_size <= parse_ptr, "stack_size <= parse_ptr before adding in stack"
                    parse[stack_size] = nb
                    stack_size += 1

    # print(f'Parse pointer: {parse_ptr}')
    assert parse_ptr == -1, "missing nodes in parse"

def create_bounds_from_boundary_type(boundary_type, shape):
    bounds = np.zeros(shape, dtype=bool)

    if boundary_type == "all_sides":
        for idx in [0, -1, (slice(None), 0), (slice(None), -1)]:
            bounds[idx] = True
    elif boundary_type == "single_side":
        bounds[0] = True
    elif boundary_type == "single_node":
        bounds[0, bounds.shape[0] // 2] = True
    else:
        raise ValueError(f"Invalid boundary type: {boundary_type}")

    return bounds
