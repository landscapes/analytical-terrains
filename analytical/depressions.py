#
# Copyright (C) 2024, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
#
# This software is free for non-commercial, research and evaluation use
# under the terms of the LICENSE.md file.
#
# For inquiries contact  petros.tzathas@inria.fr
#

import numpy as np
import numba

from analytical.flow_routing import flow_routing


def fill_holes(topo):
    result = np.empty_like(topo.z).reshape(-1)
    flow_routing.compute_water_level(topo.z.flatten(), topo.shape[0], topo.shape[1], 1, 1, 
                                    result, active_nodes = ~topo.bounds.reshape(-1), method='mst_linear', 
                                    randomize = False, directions = 4, sediment_slope = 1e-3)
    return result.reshape(topo.z.shape)


def fill_holes_from_topo(topo, z, eps = 1e-3):
    new_z = z.copy().reshape(-1)
    _numba_fill_holes_from_topo(new_z, topo.stack()[-1::-1], topo.rcv(), eps)
    return new_z.reshape(z.shape)


@numba.njit(cache = True)
def _numba_fill_holes_from_topo(z, stack, rcv, eps):

    for istack in stack:

        irec = rcv[istack, 0]

        if irec == istack:
            continue

        z[istack] = max(z[istack], z[irec]+eps)
