#
# Copyright (C) 2024, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
#
# This software is free for non-commercial, research and evaluation use
# under the terms of the LICENSE.md file.
#
# For inquiries contact  petros.tzathas@inria.fr
#

import numpy as np
import numba


def solve_analytically(
    initial_elevation,
    drainage_area,
    uplift,
    k_spl,
    k_hill,
    k_thermal,
    m_spl,
    m_hill,
    dx,
    t,
    topo,
    advection_speed_fn,
    # Rate of elevetion decrease of outlet. Used for the canyon example
    out_slope=0.0,
):
    initial_elevation = initial_elevation.reshape(-1)
    drainage_area     = drainage_area.reshape(-1)
    uplift            = uplift.reshape(-1)
    k_spl             = k_spl.reshape(-1)
    k_hill            = k_hill.reshape(-1)
    k_thermal         = k_thermal.reshape(-1)
    m_spl             = m_spl.reshape(-1)
    m_hill            = m_hill.reshape(-1)
    dx                = dx.reshape(-1)

    receivers = topo.rcv()
    stack = topo.stack()[-1::-1]

    advection_speed = advection_speed_fn(drainage_area, k_spl, k_hill, m_spl, m_hill)

    elevation, response_time, path_length = _numba_solve_transient_analytical(
        initial_elevation,
        advection_speed,
        np.copy(uplift),
        k_thermal,
        stack,
        receivers,
        dx,
        t,
        out_slope,
    )

    return (
        elevation.reshape(topo.shape),
        response_time.reshape(topo.shape),
        path_length.reshape(topo.shape),
    )


@numba.jit(nopython=True, cache=True)
def _numba_solve_transient_analytical(
    initial_elevation,
    advection_speed,
    uplift,
    k_thermal,
    stack,
    receivers,
    dx,
    t,
    out_slope,
):
    elevation = np.empty_like(initial_elevation)

    # Contribution due to initial elevation
    initial_elevation_component = np.empty_like(initial_elevation)
    # Contribution due to uplift
    uplift_elevation_component = np.empty_like(initial_elevation)
    response_time = np.empty_like(initial_elevation)
    # The response time of the node whose information reaches a node at time t
    source_response_time = np.empty_like(initial_elevation)
    # Interpolation factor for source point
    # In paper's notation: (D_{x, t} - x_i) / dx
    source_interp_t = np.empty_like(initial_elevation)

    critical_slope = np.arctan(np.deg2rad(30))

    # The length of the unique path connecting a node to its respective outlet
    path_length = np.empty_like(initial_elevation, dtype=np.intp)

    # Holds the indices of the nodes in the path currently traversed
    # Having it be of size equal to the number of nodes is very conservative
    path_node = np.empty_like(initial_elevation, dtype=np.intp)

    # The largest path index of the node whose response time less
    # than the source_response_time
    low_source_node_path_idx = np.empty_like(initial_elevation, dtype=np.intp)

    def lerp(a, b, t):
        return a + (b - a) * t

    def setup_outlet(inode):
        path_length[inode] = 0
        path_node[0] = inode

        response_time[inode] = 0
        source_response_time[inode] = 0
        source_interp_t[inode] = 0
        low_source_node_path_idx[0] = 0

        initial_elevation_component[inode] = initial_elevation[inode]
        initial_elevation_component[inode] -= out_slope * t
        uplift_elevation_component[inode] = 0
        elevation[inode] = initial_elevation_component[inode]

    def compute_path_info(inode, ircv):
        path_length[inode] = path_length[ircv] + 1
        path_node[path_length[inode]] = inode

    def compute_source_intep_t(inode):
        low_source_idx = low_source_node_path_idx[path_length[inode]]

        low_source_node = path_node[low_source_idx]
        high_source_node = path_node[low_source_idx + 1]

        low_source_resp_time  = response_time[low_source_node]
        high_source_resp_time = response_time[high_source_node]

        # In paper notation:
        # dx/a(x) - T(D_{x-dx, t}, x_i)
        # = T(x-dx, x) - T(D_{x-dx, t}, x_i)
        # = T(0, x) - T(0, x-dx) - T(0, x_i) + T(0, D_{x-dx, t})
        # = T(0, x) - (T(0, x-dx) - T(0, D_{x-dx, t})) - T(0, x_i)
        # = T(0, x) - T(D_{x-dx, t}, x-dx) - T(0, x_i)
        # = T(0, x) - t - T(0, x_i)
        # = T(0, D_{x, t}) - T(0, x_i), which is the numerator
        # because T(D_{x-dx, t}, x-dx) = T(D_{x, t}. x) = t
        numer = source_response_time[inode] - low_source_resp_time
        denom = high_source_resp_time - low_source_resp_time
        source_interp_t[inode] = numer / denom

    def compute_source_info(inode, ircv):
        low_source_idx = low_source_node_path_idx[path_length[ircv]]

        # This condition below is equivalent to the one given in the paper:
        # T(D_{x-dx, t}, x_i) < dx / a(x)
        # <=> T(D_{x-dx, t}, x_i) < T(x-dx, x)
        # <=> T(0, xi) - T(0, D_{x-dx, t}) < T(0, x) - T(0, x-dx)
        # <=> T(0, xi) < T(0, x) - (T(0, x-dx) - T(0, D_{x-dx, t}))
        # <=> T(0, xi) < T(0, x) - t
        # <=> T(0, xi) < T(0, D_{x, t}), which is the condition checked below
        # because T(D_{x-dx, t}, x-dx) = T(D_{x, t}. x) = t
        while (response_time[path_node[low_source_idx + 1]] < source_response_time[inode]):
            low_source_idx += 1

        low_source_node_path_idx[path_length[inode]] = low_source_idx

        compute_source_intep_t(inode)

    def compute_initial_elevation_component(inode):
        low_source_idx = low_source_node_path_idx[path_length[inode]]

        low_source_node = path_node[low_source_idx]
        high_source_node = path_node[low_source_idx + 1]

        initial_elevation_component[inode] = lerp(
            initial_elevation[low_source_node],
            initial_elevation[high_source_node],
            source_interp_t[inode],
        )

    def compute_partial_integral(inode):
        if path_length[inode] == 0:
            return 0

        low_source_idx = low_source_node_path_idx[path_length[inode]]

        low_node = path_node[low_source_idx]
        high_node = path_node[low_source_idx + 1]

        interpolation_t = source_interp_t[inode]
        integral = (
            (1 - interpolation_t)
            * dx[high_node]
            * uplift[high_node]
            / advection_speed[high_node]
        )

        return integral

    def compute_uplift_elevation_component(inode, ircv):
        uplift_elevation_component[inode] = uplift_elevation_component[ircv]

        node_uplift = uplift[inode]
        node_advection_speed = advection_speed[inode]
        uplift_elevation_component[inode] += (
            dx[inode] * node_uplift / node_advection_speed
        )

        if source_response_time[inode] == 0:
            return

        uplift_elevation_component[inode] -= compute_partial_integral(ircv)

        rcv_low_source_idx = low_source_node_path_idx[path_length[ircv]]

        low_node = path_node[min(rcv_low_source_idx + 1, path_length[ircv])]

        integration_idx_range = range(
            min(rcv_low_source_idx + 2, path_length[inode]),
            min(low_source_node_path_idx[path_length[inode]] + 1, path_length[inode]) + 1
        )
        for high_node_idx in integration_idx_range:
            high_node = path_node[high_node_idx]
            high_uplift = uplift[high_node]
            high_advection_speed = advection_speed[high_node]
            uplift_elevation_component[inode] -= (
                dx[high_node] * high_uplift / high_advection_speed
            )

            low_node = high_node

        uplift_elevation_component[inode] += compute_partial_integral(inode)

    def compute_elevation(inode, ircv):
        compute_path_info(inode, ircv)
        response_time[inode] = response_time[ircv] + dx[inode] / advection_speed[inode]
        source_response_time[inode] = max(0, response_time[inode] - t)
        compute_source_info(inode, ircv)
        compute_initial_elevation_component(inode)
        compute_uplift_elevation_component(inode, ircv)

        temp = response_time[inode] - t
        if temp < 0:
            initial_elevation_component[inode] += out_slope * temp

        elevation[inode] = (
            initial_elevation_component[inode] + uplift_elevation_component[inode]
        )
        # elevation[inode] = initial_elevation_component[inode]
        # elevation[inode] = uplift_elevation_component[inode]

        elevation[inode] = max(elevation[inode], elevation[ircv])

    for istack in stack:
        ircv = receivers[istack, 0]

        if ircv == istack: # If this is an outlet node
            setup_outlet(istack)
            continue

        compute_elevation(istack, ircv)

        # If above critical slope, recompute
        slope = (elevation[istack] - elevation[ircv]) / dx[istack]
        if slope >= critical_slope:
            advection_speed[istack] += k_thermal[istack]
            uplift[istack] = uplift[istack] + k_thermal[istack] * critical_slope
            compute_elevation(istack, ircv)

    return elevation, response_time, path_length
