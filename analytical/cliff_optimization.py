#
# Copyright (C) 2024, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
#
# This software is free for non-commercial, research and evaluation use
# under the terms of the LICENSE.md file.
#
# For inquiries contact  petros.tzathas@inria.fr
#

import numpy as np
import numba


def optimize_cliffs(
    target_elevation,
    topo,
    n_iters,
    learning_rate=0.01,
    river_term_factor=1,
    cliff_term_factor=2,
    normalize_factors=True,
):
    target_elevation = target_elevation.reshape(-1)

    elevation_max = np.max(target_elevation)
    elevation_min = np.min(target_elevation)
    elevation_range = elevation_max - elevation_min
    if elevation_range == 0:
        elevation_range = 1
    target_elevation = (target_elevation - elevation_min) / elevation_range

    elevation = np.copy(target_elevation)

    receivers = topo.rcv()
    stack = topo.stack()
    resolution = topo.shape[0]

    target_elevation_diffs = _compute_elevation_diffs(elevation, receivers)
    upstream_tree_size, n_donors = _compute_upstream_tree_size_and_n_donors(
        stack, receivers
    )

    normalization_factor = (
        river_term_factor + cliff_term_factor if normalize_factors else 1
    )
    river_term_factor /= normalization_factor
    cliff_term_factor /= normalization_factor

    for i in range(n_iters):
        elevation_diffs = _compute_elevation_diffs(elevation, receivers)
        river_term_grad = _compute_river_term_grad_wrt_elevation_diffs(
            elevation_diffs, target_elevation_diffs, receivers
        )
        cliff_term_grad = _compute_cliff_term_grad_wrt_elevation_diffs(
            elevation, target_elevation_diffs, stack, receivers, resolution
        )
        cliff_term_grad /= upstream_tree_size

        full_grad = (
            river_term_factor * river_term_grad + cliff_term_factor * cliff_term_grad
        )

        elevation = _accumulate_grad_and_elevation_diffs(
            elevation_diffs,
            target_elevation_diffs,
            full_grad,
            learning_rate,
            stack[-1::-1],
            receivers,
        )

    elevation = elevation * elevation_range + elevation_min
    return elevation.reshape(topo.shape)


@numba.njit(cache=True)
def _compute_upstream_tree_size_and_n_donors(stack, receivers):
    upstream_tree_size = np.ones_like(stack)
    n_donors = np.zeros_like(stack)

    for istack in stack:
        ircv = receivers[istack, 0]
        
        if istack != ircv:
            upstream_tree_size[ircv] += upstream_tree_size[istack]
            n_donors[ircv] += 1
    
    return upstream_tree_size, n_donors


@numba.njit(cache=True)
def _compute_elevation_diffs(elevation, receivers):
    elevation_diffs = np.empty_like(elevation)
    
    for inode in range(len(elevation)):
        ircv = receivers[inode, 0]
        
        if inode == ircv:
            elevation_diffs[inode] = elevation[inode]
        else:
            elevation_diffs[inode] = elevation[inode] - elevation[ircv]
    
    return elevation_diffs


@numba.njit(cache=True)
def _compute_river_term_grad_wrt_elevation_diffs(
    elevation_diffs, target_elevation_diffs, receivers
):
    loss_grad = np.zeros_like(elevation_diffs)

    for inode in range(len(elevation_diffs)):
        ircv = receivers[inode, 0]

        if inode == ircv:
            continue

        loss_grad[inode] = elevation_diffs[inode] - target_elevation_diffs[inode]

    return loss_grad


@numba.njit(cache=True)
def _compute_cliff_term_grad_wrt_elevation_diffs(
    elevation, target_elevation_diffs, stack, receivers, resolution
):
    loss_grad = np.zeros_like(elevation)

    dirs = np.array([(-1, 0), (+1, 0), (0, -1), (0, +1)], dtype=receivers.dtype)

    for inode in stack:
        inode_y, inode_x = divmod(inode, resolution)

        for dir_x, dir_y in dirs:
            neighbor_x = inode_x + dir_x
            neighbor_y = inode_y + dir_y
            neighbor_idx = neighbor_y * resolution + neighbor_x

            if (
                0 <= neighbor_idx < len(elevation)  # In bounds
                and 0 <= neighbor_x < resolution    # Same column
            ):
                if (
                    receivers[neighbor_idx, 0] != inode
                    and neighbor_idx != receivers[inode, 0]
                ):
                    neighbor_elevation_diff = elevation[inode] - elevation[neighbor_idx]

                    if neighbor_elevation_diff > 0:
                        if receivers[inode, 0] == inode:
                            loss_grad[inode] += neighbor_elevation_diff
                        else:
                            loss_grad[inode] += max(
                                neighbor_elevation_diff - target_elevation_diffs[inode],
                                0,
                            )
                    else:
                        if receivers[neighbor_idx, 0] == neighbor_idx:
                            loss_grad[inode] += neighbor_elevation_diff
                        else:
                            loss_grad[inode] += min(
                                neighbor_elevation_diff
                                + target_elevation_diffs[neighbor_idx],
                                0,
                            )

        ircv = receivers[inode, 0]

        if inode != ircv:
            loss_grad[ircv] += loss_grad[inode]

    return loss_grad


@numba.njit(cache=True)
def _accumulate_grad_and_elevation_diffs(
    previous_elevation_diffs,
    target_elevation_diffs,
    grad_wrt_elevation_diffs,
    learning_rate,
    stack,
    receivers,
):
    elevation = np.empty_like(previous_elevation_diffs)

    # Compute per node maximum learning rates so that no elevation difference
    # becomes negative (or less than the target value if is negative to begin with)
    per_node_learning_rate = np.full_like(previous_elevation_diffs, learning_rate)
    for inode in range(len(previous_elevation_diffs)):
        ircv = receivers[inode, 0]

        if inode == ircv:
            continue

        if grad_wrt_elevation_diffs[inode] > 0:
            temp = previous_elevation_diffs[inode] - min(
                target_elevation_diffs[inode], 0
            )
            per_node_learning_rate[inode] = min(
                temp / grad_wrt_elevation_diffs[inode], learning_rate
            )

    # Gradient descent step
    for istack in stack:
        ircv = receivers[istack, 0]

        if istack == ircv:
            elevation[istack] = previous_elevation_diffs[istack]
            continue

        # Update elevation diffrences and elevation
        updated_elevation_diff = (
            previous_elevation_diffs[istack]
            - per_node_learning_rate[inode] * grad_wrt_elevation_diffs[istack]
        )
        updated_elevation_diff = max(
            updated_elevation_diff, min(target_elevation_diffs[istack], 0)
        )

        elevation[istack] = elevation[ircv] + updated_elevation_diff

    return elevation
