#
# Copyright (C) 2024, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
#
# This software is free for non-commercial, research and evaluation use
# under the terms of the LICENSE.md file.
#
# For inquiries contact  petros.tzathas@inria.fr
#

import numba


def compute_drain(topo, dx, precip):
    area = precip.copy().reshape(-1) * dx * dx

    _numba_accum_flux(topo.weights(), topo.rcv(), topo.stack(), area)

    return area.reshape(topo.shape)


@numba.njit(cache=True)
def _numba_accum_flux(W, rcv, parse, q):
    for node in parse:
        for r, w in zip(rcv[node], W[node]):
            q[r] += w * q[node]
